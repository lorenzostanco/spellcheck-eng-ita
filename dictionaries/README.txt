Dictionaries extracted from:
- https://addons.mozilla.org/en-US/firefox/addon/dizionario-italiano/
- https://addons.mozilla.org/en-US/firefox/addon/us-english-dictionary/

Found from:
- https://addons.mozilla.org/it/firefox/language-tools/

Italian DIC file has been modified removing comment lines, because of:
- https://github.com/arty-name/hunspell-merge/issues/9