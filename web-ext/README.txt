This is a spell-check dictionary created by merging:
- https://addons.mozilla.org/en-US/firefox/addon/dizionario-italiano/
- https://addons.mozilla.org/en-US/firefox/addon/us-english-dictionary/

With:
- https://github.com/arty-name/hunspell-merge